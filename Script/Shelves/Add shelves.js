var ObjectId = global.get('ObjectId')
payload = msg.payload;
shelves = msg.shelves;
msg.res = msg.request.res;
// Exit 1 is for mongodb node
// Exit 2 is for response node
if (payload.length > 0) { // check if already exist in dabase
    if (payload.delete) { // check if the attribute delete is true, in this case just update the database.
        msg.query = {
            "_id": ObjectId(payload._id)
        }
        msg.payload = {
            "ray": shelves.ray,
            "s1": shelves.s1,
            "s2": shelves.s2,
            "s3": shelves.s3,
            "s4": shelves.s4,
            "s5": shelves.s5,
            "s6": shelves.s6,
            "s7": shelves.s7,
            "s8": shelves.s8,
            "delete": false
        }
        msg1 = msg;
        msg1.payload = "Created in database";
        return [msg, msg1]
    } 
    else {
        msg.payload = "Already exist in database";
        return [null, msg]
    }
}
else {
    msg.query = {
        "_id": new ObjectId()
    }
    msg.payload = {
        "ray": shelves.ray,
        "s1": shelves.s1,
        "s2": shelves.s2,
        "s3": shelves.s3,
        "s4": shelves.s4,
        "s5": shelves.s5,
        "s6": shelves.s6,
        "s7": shelves.s7,
        "s8": shelves.s8,
        "delete": false
    }
    msg1 = {};
    msg1.payload = "Created in database";
    msg1.res = msg.res;
    return [msg, msg1]  
}
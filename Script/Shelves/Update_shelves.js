var ObjectId = global.get('ObjectId');
shelves = msg.shelves;
payload = msg.payload;
msg.res = msg.request.res;
delete msg.request;

if(payload.length > 0){
    id = msg.payload._id;
    if(payload.delete){
        msg.payload = "Doesn't exist in database";
        return ([null, msg])
    }
    else {
        msg.query = {
            "_id": ObjectId(payload._id)
        }
        msg.payload = {
            "ray": shelves.ray,
            "s1": shelves.s1,
            "s2": shelves.s2,
            "s3": shelves.s3,
            "s4": shelves.s4,
            "s5": shelves.s5,
            "s6": shelves.s6,
            "s7": shelves.s7,
            "s8": shelves.s8,
            "delete": false
        }
        msg1 = msg;
        msg1.payload = "Updated in database";
        return([msg, msg1]);
    }
}
else {
    msg.payload = "Doesn't exist in database";
    return([null, msg])
}
var ObjectId = global.get('ObjectId');
request = msg.request;
body = request.req.body;
payload = msg.payload;
msg = {};
msg.res = request.res;

if (payload.length > 0){
    payload = payload[0];
    if (payload.delete){
        msg.query = {
            "_id": ObjectId(payload._id)
        }
        msg.payload = {
            "employee_id": body.employee_id,
            "producer_id": body.producer_id,
            "Date": body.Date,
            "delete": false,
            "listProducts": body.listProducts,
            "creation_date" : payload.creation_date, 
            "last_update": Date.now() / 1000
        }
        user_response = {};
        user_response.payload = "Created in database";
        user_response.res = msg.res;
        return([msg, user_response])
    }
    else {
        msg.payload = "Already Create in database"
        return([null, msg])
    }
}
else {
    creation_date = Date.now() / 1000;
    msg.query = {
        "_id": new ObjectId()
    }
    msg.payload = {
        "employee_id": body.employee_id,
        "producer_id": body.producer_id,
        "Date": body.Date,
        "delete": false,
        "listProducts": body.listProducts,
        "creation_date" : creation_date, 
        "last_update": creation_date
    }
    user_response = {};
    user_response.payload = "Created in database";
    user_response.res = msg.res;
    return([msg, user_response])
}
request = {};
request.res = msg.res;
request.req = msg.req;
body = msg.req.body;
msg = {};
payload = {};
var keys = Object.keys(body);
for (let i = 0; i < keys.length; i++){
    payload[`${keys[i].toString()}`] = body[`${keys[i].toString()}`];
}
msg.payload = payload;
msg.sort = {"_id":-1};
msg.request = request;
return msg;
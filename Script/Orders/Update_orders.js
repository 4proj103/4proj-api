var ObjectId = global.get('ObjectId');
request = msg.request;
body = request.req.body;
payload = msg.payload;
msg = {};
msg.res = request.res;

if (payload.length > 0){
    payload = payload[0];
    if (payload.delete){
        msg.payload = "Doesn't exist in database"
        return([null, msg])
    }
    else {
        msg.query = {
            "_id": ObjectId(payload._id)
        }
        msg.payload = {
            "employee_id": body.employee_id,
            "producer_id": body.producer_id,
            "Date": body.Date,
            "delete": false,
            "listProducts": body.listProducts,
            "creation_date" : payload.creation_date, 
            "last_update": Date.now() / 1000
        }
        user_response = {};
        user_response.payload = "Updated in database";
        user_response.res = msg.res;
        return([msg, user_response])
    }
}
else {
    msg.payload = "Doesn't exist in database"
    return([null, msg])
}
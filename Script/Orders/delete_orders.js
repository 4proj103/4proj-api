var ObjectId = global.get('ObjectId');
request = msg.request;
body = request.req.body;
payload = msg.payload;
msg = {};
msg.res = request.res;

if (payload.length > 0){
    payload = payload[0];
    if (payload.delete){
        msg.payload = "Doesn't exist in database"
        return([msg, null])
    }
    else {
        msg.query = {
            "_id": ObjectId(payload._id)
        }
        msg.payload = {
            "employee_id": payload.employee_id,
            "producer_id": payload.producer_id,
            "Date": payload.Date,
            "delete": true,
            "listProducts": payload.listProducts,
            "creation_date" : payload.creation_date, 
            "last_update": Date.now() / 1000
        }
        user_response = {};
        user_response.payload = "Deleted in database";
        user_response.res = msg.res;
        return([user_response, msg])
    }
}
else {
    msg.payload = "Doesn't exist in database";
    return([msg, null])
}
var ObjectId = global.get('ObjectId');
request = msg.request;
payload = msg.payload;
body = request.req.body;
msg = {};
msg.res = request.res;

if (payload.length > 0){
    payload = payload[0];
    if(payload.delete){
        msg.payload = "Doesn't exist in database";
        return([msg, null])
    }
    else{
        msg.query = {
            "_id": payload._id
        }
        msg.payload = {
            "mail": payload.mail,
            "name": payload.name,
            "last_name": payload.last,
            "sex": payload.sex,
            "birthdate" : payload.birthdate,
            "history_commands": "meh",
            "credit_cards": {
                "CardNumber": payload.credit_cards.CardNumber,
                "ExpirationDate": payload.credit_cards.ExpirationDate,
                "Crypto": payload.credit_cards.Crypto
            },
            "address": payload.address,
            "city": payload.city,
            "postalCode": payload.postalCode,
            "id_freezer": "meh1",
            "location": "meh2",
            "creation_date" : payload.creation_date,
            "last_update": Date.now() / 1000,
            "delete": true
        }
        user_response = {};
        user_response.payload = "Deleted in Database";
        user_response.res = msg.res;
        return([user_response, msg])
    }
}
else{
    msg.payload = "Doesn't exist in database";
    return([msg, null])
}
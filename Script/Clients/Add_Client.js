var ObjectId = global.get('ObjectId');
request = msg.request;
body = request.req.body;
payload = msg.payload;
msg = {};
msg.res = request.res;
if (payload.length > 0){
    payload = payload[0];
    if (payload.delete){
        msg.query = {
            "_id": ObjectId(payload._id)
        }
        msg.payload = {
            "mail": body.mail,
            "name": body.name,
            "last_name": body.last_name,
            "sex": body.sex,
            "birthdate" : body.birthdate,
            "history_commands": "meh",
            "credit_cards": {
                "CardNumber": body.credit_cards.CardNumber,
                "ExpirationDate": body.credit_cards.ExpirationDate,
                "Crypto": body.credit_cards.Crypto
            },
            "address": body.address,
            "city": body.city,
            "postalCode": body.postalCode,
            "id_freezer": "meh1",
            "location": "meh2",
            "delete": false,
            "creation_date" : payload.creation_date, 
            "last_update": Date.now() / 1000
        }
        msg1 = {};
        msg1.payload = "Created in database";
        msg1.res = msg.res;
        return ([msg, msg1])
    }
    else {
        msg.payload = "Already Created in database";
        return([null, msg]);
    }
}
else {
    msg.query = {
        "_id": new ObjectId()
    };
    creation_date = Date.now() / 1000;
    msg.payload = {
        "mail": body.mail,
        "name": body.name,
        "last_name": body.last_name,
        "sex": body.sex,
        "birthdate" : body.birthdate,
        "history_commands": "meh",
        "credit_cards": {
            "CardNumber": body.credit_cards.CardNumber,
            "ExpirationDate": body.credit_cards.ExpirationDate,
            "Crypto": body.credit_cards.Crypto
        },
        "address": body.address,
        "city": body.city,
        "postalCode": body.postalCode,
        "id_freezer": "meh1",
        "location": "meh2",
        "delete": false,
        "creation_date" : creation_date, 
        "last_update": creation_date
    }
    msg1 = {};
    msg1.payload = "Created in database";
    msg1.res = msg.res;
    return([msg, msg1])
}